package com.rostyslavprotsiv.controller;

import com.rostyslavprotsiv.model.action.DOM.DOMParserDemonstration;
import com.rostyslavprotsiv.model.action.SAX.SAXParserDemonstration;
import com.rostyslavprotsiv.model.action.StAX.StAXParserDemonstration;
import com.rostyslavprotsiv.model.action.Validator;
import com.rostyslavprotsiv.model.action.transformer.HTMLTransformer;
import com.rostyslavprotsiv.model.entity.Knifes;
import com.rostyslavprotsiv.view.Menu;
import com.sun.org.apache.bcel.internal.generic.NEW;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;


public class Controller {
    private static final Logger LOGGER = LogManager.getLogger(Controller.class);
    private static final Validator VALIDATOR = new Validator();
    private static final Menu MENU = new Menu();
    private static final String XML_FILE = ".\\src\\main\\resources\\xml"
            + "\\cold_steel.xml";
    private static final String XSD_FILE = ".\\src\\main\\resources\\xml"
            + "\\cold_steel.xsd";
    private static final String XSL_FILE = ".\\src\\main\\resources\\xml"
            + "\\cold_steel.xsl";
    private static final String ROOT_XSL_FILE = ".\\src\\main\\resources\\xml"
            + "\\cold_steel_new_root.xsl";
    private static final String NEW_HTML_FILE = ".\\src\\main\\resources\\xml"
            + "\\cold_steel.html";
    private static final String NEW_XML_FILE = ".\\src\\main\\resources\\xml"
            + "\\cold_steel_new.xml";
    private static final SAXParserDemonstration SAX_PARSER_DEMONSTRATION =
            new SAXParserDemonstration();
    private static final DOMParserDemonstration DOM_PARSER_DEMONSTRATION =
            new DOMParserDemonstration();
    private static final StAXParserDemonstration STAX_PARSER_DEMONSTRATION =
            new StAXParserDemonstration();
    private static final HTMLTransformer HTML_TRANSFORMER =
            new HTMLTransformer();


    public void execute() {
        Knifes knifes = new Knifes();
        MENU.welcome();
//        MENU.validation();
//        VALIDATOR.validate(XML_FILE, XSD_FILE);

//        knifes.getKnife().addAll(SAX_PARSER_DEMONSTRATION
//                .demonstrate(XML_FILE));
//        MENU.saxParserDemonstrate(knifes.toString());

//        knifes.getKnife().addAll(DOM_PARSER_DEMONSTRATION
//                .demonstrate(XML_FILE));
//        MENU.domParserDemonstrate(knifes.toString());

//        knifes.getKnife().addAll(STAX_PARSER_DEMONSTRATION
//                .demonstrate(XML_FILE));
//        MENU.staxParserDemonstrate(knifes.toString());
//        knifes = new Knifes();

//        MENU.htmlTransformationDemonstrate();
//        HTML_TRANSFORMER.transform(XSL_FILE, NEW_HTML_FILE, XML_FILE);

//        knifes.getKnife().addAll(STAX_PARSER_DEMONSTRATION
//                .demonstrateSorted(XML_FILE));
//        MENU.staxParserDemonstrateSorted(knifes.toString());

          MENU.xmlTransformationDemonstrate();
        HTML_TRANSFORMER.transform(ROOT_XSL_FILE, NEW_XML_FILE, XML_FILE);
    }

}

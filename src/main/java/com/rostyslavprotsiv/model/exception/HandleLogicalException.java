package com.rostyslavprotsiv.model.exception;

public class HandleLogicalException extends Exception {
    public HandleLogicalException() {}

    public HandleLogicalException(String message, Throwable cause) {
        super(message, cause);
    }

    public HandleLogicalException(String message) {
        super(message);
    }

    public HandleLogicalException(Throwable cause) {
        super(cause);
    }
}

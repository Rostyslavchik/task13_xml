package com.rostyslavprotsiv.model.action.handler;

import org.xml.sax.helpers.DefaultHandler;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.xml.sax.SAXParseException;

import java.io.IOException;


public class ColdSteelErrorHandler extends DefaultHandler {
        private static Logger logger = LogManager
                .getLogger(ColdSteelErrorHandler.class);

        public void warning(SAXParseException e) {
            logger.warn(getLineAddress(e) + "-" + e.getMessage());
        }
        public void error(SAXParseException e) {
            logger.error(getLineAddress(e) + " - " + e.getMessage());
        }
        public void fatalError(SAXParseException e) {
            logger.fatal(getLineAddress(e) + " - " + e.getMessage());
        }
        private String getLineAddress(SAXParseException e) {
            return e.getLineNumber() + " : " + e.getColumnNumber();
        }
}

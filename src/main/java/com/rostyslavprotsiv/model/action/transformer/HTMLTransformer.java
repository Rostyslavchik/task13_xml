package com.rostyslavprotsiv.model.action.transformer;

import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;

public class HTMLTransformer {
    public void transform(String xslFile, String newHtmlPath, String xmlFile) {
        try {
            TransformerFactory tf = TransformerFactory.newInstance();
// установка используемого XSL-преобразования
            Transformer transformer = tf.newTransformer(new StreamSource(xslFile));

// установка исходного XML-документа и конечного XML-файла
            transformer.transform(new StreamSource(xmlFile),
                    new StreamResult(newHtmlPath));
            System.out.println("Transformation " + newHtmlPath + " complete");
        } catch(TransformerException e) {
            System.err.println("Impossible transform file "
                    + newHtmlPath + " : " + e);
        }
    }
}

package com.rostyslavprotsiv.model.action.StAX;

import com.rostyslavprotsiv.model.action.comparator.LengthComparator;
import com.rostyslavprotsiv.model.entity.Blade;
import com.rostyslavprotsiv.model.entity.Knife;

import java.util.Collections;
import java.util.Comparator;
import java.util.LinkedHashSet;
import java.util.Set;
import java.util.stream.Collectors;

public class StAXParserDemonstration {
    public Set<Knife> demonstrate(String fileName) {
        ColdSteelStAXBuilder builder = new ColdSteelStAXBuilder();
        builder.buildSetKnifes(fileName);
        return builder.getKnifes();
    }

    public Set<Knife> demonstrateSorted(String fileName) {
        Set<Knife> sorted = demonstrate(fileName);
        return sorted.stream()
                .sorted(new LengthComparator())
                .collect(Collectors.toCollection(LinkedHashSet::new));
    }
}

package com.rostyslavprotsiv.model.action.StAX;

import com.rostyslavprotsiv.model.action.SAX.ColdSteelEnum;
import com.rostyslavprotsiv.model.entity.*;
import com.rostyslavprotsiv.model.exception.HandleLogicalException;
import com.sun.deploy.security.ValidationState;

import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamConstants;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.HashSet;
import java.util.Set;

public class ColdSteelStAXBuilder {
    private HashSet<Knife> knifes = new HashSet<>();
    private XMLInputFactory inputFactory;

    public ColdSteelStAXBuilder() {
        inputFactory = XMLInputFactory.newInstance();
    }

    public Set<Knife> getKnifes() {
        return knifes;
    }

    public void buildSetKnifes(String fileName) {
        FileInputStream inputStream = null;
        XMLStreamReader reader = null;
        String name;
        try {
            inputStream = new FileInputStream(new File(fileName));
            reader = inputFactory.createXMLStreamReader(inputStream);
            // StAX parsing
            while (reader.hasNext()) {
                int type = reader.next();
                if (type == XMLStreamConstants.START_ELEMENT) {
                    name = reader.getLocalName();
                    if (ColdSteelEnum.valueOf(name.toUpperCase())
                            == ColdSteelEnum.KNIFE) {
                        Knife kn = buildKnife(reader);
                        knifes.add(kn);
                    }
                }
            }
        } catch (XMLStreamException ex) {
            System.err.println("StAX parsing error! " + ex.getMessage());
        } catch (FileNotFoundException ex) {
            System.err.println("File " + fileName + " not found! " + ex);
        } catch (HandleLogicalException e) {
            e.printStackTrace();
        } finally {
            try {
                if (inputStream != null) {
                    inputStream.close();
                }
            } catch (IOException e) {
                System.err.println("Impossible close file " + fileName + " : " + e);
            }
        }
    }

    private Knife buildKnife(XMLStreamReader reader)
            throws XMLStreamException, HandleLogicalException {
        Knife kn = new Knife();
        String name;
        while (reader.hasNext()) {
            int type = reader.next();
            switch (type) {
                case XMLStreamConstants.START_ELEMENT:
                    name = reader.getLocalName();
                    switch (ColdSteelEnum.valueOf(name.toUpperCase())) {
                        case TYPE:
                            kn.setType(Type.valueOf(getXMLText(reader)
                                    .replaceAll("[ -]+",
                                            "_").toUpperCase()));
                            break;
                        case HANDY:
                            kn.setHandy(Handy.valueOf(getXMLText(reader)
                                    .replaceAll("[ -]+",
                                            "_").toUpperCase()));
                            break;
                        case ORIGIN:
                            kn.setOrigin(getXMLText(reader));
                            break;
                        case VISUAL:
                            kn.setVisual(getXMLVisual(reader));
                            break;
                        case VALUE:
                            kn.setValue(getXMLValue(reader));
                            break;
                    }
                    break;
                case XMLStreamConstants.END_ELEMENT:
                    name = reader.getLocalName();
                    if (ColdSteelEnum.valueOf(name.toUpperCase())
                            == ColdSteelEnum.KNIFE) {
                        return kn;
                    }
                    break;
            }
        }
        throw new XMLStreamException("Unknown element in tag Knife");
    }

    private Visual getXMLVisual(XMLStreamReader reader)
            throws XMLStreamException, HandleLogicalException {
        Visual visual = new Visual();
        int type;
        String name;
        while (reader.hasNext()) {
            type = reader.next();
            switch (type) {
                case XMLStreamConstants.START_ELEMENT:
                    name = reader.getLocalName();
                    switch (ColdSteelEnum.valueOf(name.toUpperCase())) {
                        case BLADE:
                            visual.setBlade(getXMLBlade(reader));
                            break;
                        case HANDLE:
                            visual.setHandle(getXMLHandle(reader));
                            break;
                        case BLOOD_GROOVE:
                            visual.setBloodGroove(Boolean
                                    .parseBoolean(getXMLText(reader)));
                            break;
                    }
                    break;
                case XMLStreamConstants.END_ELEMENT:
                    name = reader.getLocalName();
                    if (ColdSteelEnum.valueOf(name.toUpperCase())
                            == ColdSteelEnum.VISUAL) {
                        return visual;
                    }
                    break;
            }
        }
        throw new XMLStreamException("Unknown element in tag Visual");
    }

    private Blade getXMLBlade(XMLStreamReader reader)
            throws XMLStreamException {
        Blade blade = new Blade();
        int type;
        String name;
        while (reader.hasNext()) {
            type = reader.next();
            switch (type) {
                case XMLStreamConstants.START_ELEMENT:
                    name = reader.getLocalName();
                    switch (ColdSteelEnum.valueOf(name.toUpperCase())) {
                        case LENGTH:
                            blade.setLength(getXMLText(reader));
                            break;
                        case WIDTH:
                            blade.setWidth(getXMLText(reader));
                            break;
                        case MATERIAL:
                            blade.setMaterial(Material.valueOf(
                                    getXMLText(reader).replaceAll("[ -]+",
                                    "_").toUpperCase()));
                            break;
                    }
                    break;
                case XMLStreamConstants.END_ELEMENT:
                    name = reader.getLocalName();
                    if (ColdSteelEnum.valueOf(name.toUpperCase())
                            == ColdSteelEnum.BLADE) {
                        return blade;
                    }
                    break;
            }
        }
        throw new XMLStreamException("Unknown element in tag Blade");
    }

    private Handle getXMLHandle(XMLStreamReader reader)
            throws XMLStreamException, HandleLogicalException {
        Handle handle = new Handle();
        int type;
        String name;
        handle.setWooden(Boolean.parseBoolean(reader.getAttributeValue(null,
                ColdSteelEnum.WOODEN.getValue())));
        while (reader.hasNext()) {
            type = reader.next();
            switch (type) {
                case XMLStreamConstants.START_ELEMENT:
                    name = reader.getLocalName();
                    if (ColdSteelEnum.valueOf(name.toUpperCase())
                            == ColdSteelEnum.MATERIAL) {
                            handle.setMaterial(HandleMaterial.valueOf(
                                    getXMLText(reader).replaceAll("[ -]+",
                                            "_").toUpperCase()));
                            break;
                    }
                    break;
                case XMLStreamConstants.END_ELEMENT:
                    name = reader.getLocalName();
                    if (ColdSteelEnum.valueOf(name.toUpperCase())
                            == ColdSteelEnum.HANDLE) {
                        return handle;
                    }
                    break;
            }
        }
        throw new XMLStreamException("Unknown element in tag Handle");
    }

    private Value getXMLValue(XMLStreamReader reader)
            throws XMLStreamException {
        Value value = new Value();
        int type;
        String name;
        while (reader.hasNext()) {
            type = reader.next();
            switch (type) {
                case XMLStreamConstants.START_ELEMENT:
                    name = reader.getLocalName();
                    if (ColdSteelEnum.valueOf(name.toUpperCase())
                            == ColdSteelEnum.COLLECTORS) {
                        value.setCollectors(Boolean.parseBoolean(
                                getXMLText(reader)));
                        break;
                    }
                    break;
                case XMLStreamConstants.END_ELEMENT:
                    name = reader.getLocalName();
                    if (ColdSteelEnum.valueOf(name.toUpperCase())
                            == ColdSteelEnum.VALUE) {
                        return value;
                    }
                    break;
            }
        }
        throw new XMLStreamException("Unknown element in tag Value");
    }

    private String getXMLText(XMLStreamReader reader)
            throws XMLStreamException {
        String text = null;
        if (reader.hasNext()) {
            reader.next();
            text = reader.getText();
        }
        return text;
    }
}

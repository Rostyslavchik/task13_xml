package com.rostyslavprotsiv.model.action.DOM;

import com.rostyslavprotsiv.model.entity.*;
import com.rostyslavprotsiv.model.exception.HandleLogicalException;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.IOException;
import java.util.HashSet;
import java.util.Set;

public class ColdSteelDOMBuilder {
    private Set<Knife> knifes;
    private DocumentBuilder docBuilder;

    public ColdSteelDOMBuilder() {
        this.knifes = new HashSet<Knife>();
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        try {
            docBuilder = factory.newDocumentBuilder();
        } catch (ParserConfigurationException e) {
            System.err.println("Ошибка конфигурации парсера: " + e);
        }
    }

    public Set<Knife> getKnifes() {
        return knifes;
    }

    public void buildSetKnifes(String fileName) {
        Document doc = null;
        try {
            // parsing XML-документа и создание древовидной структуры
            doc = docBuilder.parse(fileName);
            Element root = doc.getDocumentElement();
            // получение списка дочерних элементов <knife>
            NodeList knifesList = root.getElementsByTagName("knife");
            for (int i = 0; i < knifesList.getLength(); i++) {
                Element knifeElement = (Element) knifesList.item(i);
                Knife knife = buildKnife(knifeElement);
                knifes.add(knife);
            }
        } catch (IOException e) {
            System.err.println("File error or I/O error: " + e);
        } catch (SAXException e) {
            System.err.println("Parsing failure: " + e);
        } catch (HandleLogicalException e) {
            e.printStackTrace();
        }
    }

    private Knife buildKnife(Element knifeElement)
            throws HandleLogicalException {
        Knife knife = new Knife();
        // заполнение объекта knife
        knife.setType(Type.valueOf(getElementTextContent(knifeElement,
                "type")
                .replaceAll("[ -]+", "_").toUpperCase()));
        knife.setHandy(Handy.valueOf(getElementTextContent(knifeElement,
                "handy")
                .replaceAll("[ -]+", "_").toUpperCase()));
        knife.setOrigin(getElementTextContent(knifeElement,
                "origin"));
        Visual visual = knife.getVisual();
        Blade blade = visual.getBlade();
        Handle handle = new Handle();
        visual.setHandle(handle);
        Value value = knife.getValue();
        Element visualElement = (Element) knifeElement
                .getElementsByTagName("visual").item(0);
        Element bladeElement = (Element) visualElement
                .getElementsByTagName("blade").item(0);
        Element handleElement = (Element) visualElement
                .getElementsByTagName("handle").item(0);
        Element valueElement = (Element) knifeElement
                .getElementsByTagName("value").item(0);
        blade.setLength(getElementTextContent(bladeElement,
                "length"));
        blade.setWidth(getElementTextContent(bladeElement,
                "width"));
        blade.setMaterial(Material.valueOf(getElementTextContent(bladeElement,
                "material")
                .replaceAll("[ -]+", "_").toUpperCase()));
        handle.setWooden(Boolean.parseBoolean(
                handleElement.getAttribute("wooden")));
        handle.setMaterial(HandleMaterial.valueOf(getElementTextContent(
                handleElement, "material")
                .replaceAll("[ -]+", "_").toUpperCase()));
        visual.setBloodGroove(Boolean.parseBoolean(getElementTextContent(
                visualElement, "blood_groove")));
        value.setCollectors(Boolean.parseBoolean(getElementTextContent(
                valueElement, "collectors")));
        return knife;
    }

    // получение текстового содержимого тега
    private static String getElementTextContent(Element element,
                                                String elementName) {
        NodeList nList = element.getElementsByTagName(elementName);
        Node node = nList.item(0);
        String text = node.getTextContent();
        return text;
    }
}

package com.rostyslavprotsiv.model.action.DOM;

import com.rostyslavprotsiv.model.entity.Knife;

import java.util.Set;

public class DOMParserDemonstration {
    public Set<Knife> demonstrate(String fileName) {
        ColdSteelDOMBuilder coldSteelDOMBuilder = new ColdSteelDOMBuilder();
        coldSteelDOMBuilder.buildSetKnifes(fileName);
        return coldSteelDOMBuilder.getKnifes();
    }
}

package com.rostyslavprotsiv.model.action.SAX;

import com.rostyslavprotsiv.model.entity.Knife;
import org.xml.sax.SAXException;
import org.xml.sax.XMLReader;
import org.xml.sax.helpers.XMLReaderFactory;

import java.io.IOException;
import java.util.Set;

public class ColdSteelSAXBuilder {
    private Set<Knife> knifes;
    private ColdSteelHandler sh;
    private XMLReader reader;

    public ColdSteelSAXBuilder() {
        sh = new ColdSteelHandler();
        try {
            reader = XMLReaderFactory.createXMLReader();
            reader.setContentHandler(sh);
        } catch (SAXException e) {
            System.err.print("ошибка SAX парсера: " + e);
        }
    }

    public Set<Knife> getKnifes() {
        return knifes;
    }

    public void buildSetKnifes(String fileName) {
        try {
            reader.parse(fileName);
        } catch (SAXException e) {
            System.err.print("ошибка SAX парсера: " + e);
        } catch (IOException e) {
            System.err.print("ошибка I/О потока: " + e);
        }
        knifes = sh.getKnifes();
    }
}

package com.rostyslavprotsiv.model.action.SAX;

import com.rostyslavprotsiv.model.entity.*;
import com.rostyslavprotsiv.model.exception.HandleLogicalException;
import javafx.scene.image.PixelFormat;
import org.xml.sax.Attributes;
import org.xml.sax.helpers.DefaultHandler;

import java.util.EnumSet;
import java.util.HashSet;
import java.util.Set;

public class ColdSteelHandler extends DefaultHandler {
        private Set<Knife> knifes;
        private Knife current = null;
        private ColdSteelEnum currentEnum = null;
        private EnumSet<ColdSteelEnum> withText;

        public ColdSteelHandler() {
            knifes = new HashSet<Knife>();
            withText = EnumSet.range(ColdSteelEnum.KNIFE
                    , ColdSteelEnum.COLLECTORS);
        }

        public Set<Knife> getKnifes() {
            return knifes;
        }

        public void startElement(String uri, String localName, String qName,
                                 Attributes attrs) {
            if ("knife".equals(localName)) {
                current = new Knife();
            } else if ("handle".equals(localName)) {
                current.getVisual().setHandle(new Handle());
                try {
                    current.getVisual().getHandle()
                            .setWooden(Boolean.parseBoolean(
                                    attrs.getValue(0)));
                } catch (HandleLogicalException e) {
                    e.printStackTrace();
                }
            } else {
                ColdSteelEnum temp = ColdSteelEnum
                        .valueOf(localName.toUpperCase());
                if (withText.contains(temp)) {
                    currentEnum = temp;
                }
            }
        }

        public void endElement(String uri, String localName, String qName) {
            if ("knife".equals(localName)) {
                knifes.add(current);
            }
        }

        public void characters(char[] ch, int start, int length) {
            String s = new String(ch, start, length).trim();
            if (currentEnum != null) {
                switch (currentEnum) {
                    case TYPE:
                        current.setType(Type.valueOf(s.replaceAll("[ -]+",
                                "_").toUpperCase()));
                        break;
                    case HANDY:
                        current.setHandy(Handy.valueOf(s.replaceAll("[ -]+",
                                "_").toUpperCase()));
                        break;
                    case ORIGIN:
                        current.setOrigin(s);
                        break;
                    case LENGTH:
                        current.getVisual().getBlade().setLength(s);
                        break;
                    case WIDTH:
                        current.getVisual().getBlade().setWidth(s);
                        break;
                    case MATERIAL:
                        setSomeMaterial(s);
                        break;
                    case BLOOD_GROOVE:
                        current.getVisual().setBloodGroove(
                                Boolean.parseBoolean(s));
                        break;
                    case COLLECTORS:
                        current.getValue().setCollectors(
                                Boolean.parseBoolean(s));
                        break;
                    default:
                        throw new EnumConstantNotPresentException(
                                currentEnum.getDeclaringClass(),
                                currentEnum.name());
                }
            }
            currentEnum = null;
        }

        private void setSomeMaterial(String s) {
            if (current.getVisual().getHandle() == null) {
                current.getVisual().getBlade()
                        .setMaterial(Material
                                .valueOf(s.replaceAll("[ -]+",
                                        "_").toUpperCase()));
            } else {
                try {
                    current.getVisual()
                            .getHandle()
                            .setMaterial(HandleMaterial
                                    .valueOf(s.replaceAll("[ -]+",
                                            "_").toUpperCase()));
                } catch (HandleLogicalException e) {
                    e.printStackTrace();
                }
            }
        }
}

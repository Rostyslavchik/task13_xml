package com.rostyslavprotsiv.model.action.SAX;

import com.rostyslavprotsiv.model.entity.Knife;

import java.util.Set;

public class SAXParserDemonstration {
    public Set<Knife> demonstrate(String fileName) {
        ColdSteelSAXBuilder builder = new ColdSteelSAXBuilder();
        builder.buildSetKnifes(fileName);
        return builder.getKnifes();
    }
}

package com.rostyslavprotsiv.model.action.SAX;

public enum ColdSteelEnum {
    KNIFES("knifes"),
    KNIFE("knife"),
    TYPE("type"),
    HANDY("handy"),
    ORIGIN("origin"),
    LENGTH("length"),
    WIDTH("width"),
    MATERIAL("material"),
    BLOOD_GROOVE("blood_groove"),
    COLLECTORS("collectors"),
    VISUAL("visual"),
    BLADE("blade"),
    HANDLE("handle"),
    WOODEN("wooden"),
    VALUE("value");

    private String value;

    private ColdSteelEnum(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }
}

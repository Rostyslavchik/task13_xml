package com.rostyslavprotsiv.model.action.comparator;

import com.rostyslavprotsiv.model.entity.Blade;
import com.rostyslavprotsiv.model.entity.Knife;

import java.util.Comparator;

public class LengthComparator implements Comparator<Knife> {
    @Override
    public int compare(Knife first, Knife second) {
        int firstLengthStringSize = first.getVisual().getBlade().getLength().length();
        int secondLengthStringSize = second.getVisual().getBlade().getLength().length();
        int firstLength = Integer.parseInt(first.getVisual().getBlade().getLength()
                .substring(0, firstLengthStringSize - 2));
        int secondLength = Integer.parseInt(second.getVisual().getBlade().getLength()
                .substring(0, secondLengthStringSize - 2));
        return secondLength - firstLength;
    }
}

package com.rostyslavprotsiv.view;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;


public class Menu {
    private static final Logger LOGGER = LogManager.getLogger(Menu.class);

    public void welcome() {
        LOGGER.info("Welcome to my program!");
    }

    public void validation() {
        LOGGER.info("Start of validation: ");
    }

    public void saxParserDemonstrate(String objects) {
        LOGGER.info("The result of parsing into object with the help of SAX:"
                 + objects);
    }

    public void domParserDemonstrate(String objects) {
        LOGGER.info("The result of parsing into object with the help of DOM:"
                + objects);
    }

    public void staxParserDemonstrate(String objects) {
        LOGGER.info("The result of parsing into object with the help of StAX:"
                + objects);
    }

    public void staxParserDemonstrateSorted(String objects) {
        LOGGER.info("The result of parsing into sorted "
                + "objects with the help of StAX:" + objects);
    }

    public void htmlTransformationDemonstrate() {
        LOGGER.info("Check the result of parsing into html file "
                + "with the help of XSL.");
    }

    public void xmlTransformationDemonstrate() {
        LOGGER.info("Check the result of parsing into xml file with "
                + "changed root with the help of XSL.");
    }
}

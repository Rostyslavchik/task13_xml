<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0"
                xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:sm="http://www.rostyslavprotsiv.com/cold_steel">
    <xsl:output method="html"/>
    <xsl:template match="/">
        <html>
            <body>
                <h2>My Knifes</h2>
                <table border="2">
                    <tr>
                        <th>Type</th>
                        <th>Handy</th>
                        <th>Origin</th>
                        <th>Visual</th>
                        <th>Value</th>
                    </tr>
                    <xsl:for-each select="sm:knifes/sm:knife">
                        <xsl:sort select="sm:origin" order="descending"/>
                        <tr>
                            <td>
                                <xsl:value-of select="sm:type"/>
                            </td>
                            <td>
                                <xsl:value-of select="sm:handy"/>
                            </td>
                            <td>
                                <xsl:value-of select="sm:origin"/>
                            </td>
                            <td>
                                <table border="2">
                                    <tr>
                                        <th>Blade</th>
                                        <th>Handle</th>
                                        <th>Blood Groove</th>
                                    </tr>
                                    <tr>
                                        <td>
                                            <table border="2">
                                                <tr>
                                                    <th>Length</th>
                                                    <th>Width</th>
                                                    <th>Material</th>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <xsl:value-of
                                                                select="sm:visual/sm:blade/sm:length"/>
                                                    </td>
                                                    <td>
                                                        <xsl:value-of
                                                                select="sm:visual/sm:blade/sm:width"/>
                                                    </td>
                                                    <td>
                                                        <xsl:value-of
                                                                select="sm:visual/sm:blade/sm:material"/>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                        <td>
                                            <table border="2">
                                                <tr>
                                                    <th>Wooden</th>
                                                    <th>Material</th>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <xsl:value-of
                                                                select="sm:visual/sm:handle/@wooden"/>
                                                    </td>
                                                    <td>
                                                        <xsl:value-of
                                                                select="sm:visual/sm:handle/sm:material"/>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                        <td>
                                            <xsl:value-of
                                                    select="sm:visual/sm:blood_groove"/>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                            <td>
                                <table border="2">
                                    <tr>
                                        <th>Collectors</th>
                                    </tr>
                                    <tr>
                                        <td>
                                            <xsl:value-of
                                                    select="sm:value/sm:collectors"/>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </xsl:for-each>
                </table>
            </body>
        </html>
    </xsl:template>
</xsl:stylesheet>

<?xml version="1.0" encoding="UTF-8" ?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:sm="http://www.rostyslavprotsiv.com/cold_steel">
    <xsl:output method="xml" />
    <xsl:template match="@*|node()">
            <xsl:copy>
                <xsl:apply-templates select="@*|node()" />
            </xsl:copy>
    </xsl:template>
    <xsl:template match="sm:knifes">
        <mytag><xsl:apply-templates select="@*|node()" /></mytag>
    </xsl:template>
<!--    <xsl:template match="sm:knife">-->
<!--        <xsl:element name="knife">-->
<!--            <type><xsl:value-of select="type"/></type>-->
<!--            <handy><xsl:value-of select="handy"/></handy>-->
<!--            <origin><xsl:value-of select="origin"/></origin>-->
<!--            <xsl:element name="address">-->
<!--                <xsl:attribute name="country">-->
<!--                    <xsl:value-of select="address/country"/>-->
<!--                </xsl:attribute>-->
<!--                <xsl:attribute name="city">-->
<!--                    <xsl:value-of select="address/city"/>-->
<!--                </xsl:attribute>-->
<!--                <xsl:attribute name="street">-->
<!--                    <xsl:value-of select="address/street"/>-->
<!--                </xsl:attribute>-->
<!--                <xsl:element name="telephone">-->
<!--                    <xsl:attribute name="number">-->
<!--                        <xsl:value-of select="telephone"/>-->
<!--                    </xsl:attribute>-->
<!--                </xsl:element>-->
<!--            </xsl:element>-->
<!--        </xsl:element>-->
<!--    </xsl:template>-->
</xsl:stylesheet>
package com.rostyslavprotsiv.model.action.transformer;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import java.io.File;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class HTMLTransformerTest {
    private static final String XML_FILE = ".\\src\\main\\resources\\xml"
            + "\\cold_steel.xml";
    private static final String XSD_FILE = ".\\src\\main\\resources\\xml"
            + "\\cold_steel.xsd";
    private static final String XSL_FILE = ".\\src\\main\\resources\\xml"
            + "\\cold_steel.xsl";
    private static final String ROOT_XSL_FILE = ".\\src\\main\\resources\\xml"
            + "\\cold_steel_new_root.xsl";
    private static final String NEW_HTML_FILE = ".\\src\\main\\resources\\xml"
            + "\\cold_steel.html";
    private static final String NEW_XML_FILE = ".\\src\\main\\resources\\xml"
            + "\\cold_steel_new.xml";
    private static HTMLTransformer transformer;

    @BeforeAll
    static void setUp() {
        transformer = new HTMLTransformer();
    }

    @Test
    void testTransform() {
        transformer.transform(XSL_FILE, NEW_XML_FILE, XML_FILE);
        assertTrue(new File(NEW_HTML_FILE).exists());
    }
}
